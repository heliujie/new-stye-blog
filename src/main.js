/**
 * Created by aresn on 16/6/20.
 */
import 'babel-polyfill';
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './components/app.vue';
import Routers from './router';
import Util from './libs/utils';
import iView from 'iview';
import Env from './config/env';
import bus from './components/bus';
import VueBar from 'vuebar'
import VueLazyload from 'vue-lazyload'
import axios from 'axios'
import 'iview/dist/styles/iview.css';
import './styles/func.css'
import './styles/vuebar.css'


axios.defaults.timeout = 20000;                        //响应时间
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';           //配置请求头
// axios.defaults.withCredentials = true //cookie

Vue.use(VueRouter);
Vue.use(iView);
Vue.use(VueBar);
Vue.use(VueLazyload, {
  // loading: require('common/image/default.png')
})

Vue.prototype.$http = axios

// 开启debug模式
Vue.config.debug = true;

bus.$on('on-change-lang', (lang, path) => {
    // Vue.config.lang = lang;
    // bus.lang = lang;
    window.localStorage.setItem('language', lang);
    window.location.href = path;
});

// 路由配置
const RouterConfig = {
    routes: Routers
};
if (Env != 'local') {
  RouterConfig.mode = 'history';
}
const router = new VueRouter(RouterConfig);

router.beforeEach((to, from, next) => {
    iView.LoadingBar.start();
    if (to.name === 'afile') {
        Util.title(unescape(to.query.foldername))
    } else if (to.name === 'filelist') {
        Util.title(unescape(to.query.foldername))
    } else {
        Util.title(to.meta.title)
    }
    next();
});

router.afterEach((to, from, next) => {
    iView.LoadingBar.finish();
    window.scrollTo(0, 0);
});

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});
