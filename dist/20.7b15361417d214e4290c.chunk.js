webpackJsonp([20],{

/***/ 483:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(686)

var Component = __webpack_require__(82)(
  /* script */
  __webpack_require__(613),
  /* template */
  __webpack_require__(647),
  /* scopeId */
  "data-v-6a72b02e",
  /* cssModules */
  null
)
Component.options.__file = "/Volumes/My Passport/new-style-blog/src/views/m/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6a72b02e", Component.options)
  } else {
    hotAPI.reload("data-v-6a72b02e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 613:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _axios = __webpack_require__(85);

var _axios2 = _interopRequireDefault(_axios);

var _utils = __webpack_require__(86);

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            folderlist: []
        };
    },
    created: function created() {
        this.$route.name === 'mhome' ? this._loadData() : null;
    },
    mounted: function mounted() {
        _utils2.default.setViewport();
    },

    methods: {
        viewFolder: function viewFolder(item) {
            this.$router.push({
                name: 'mfilelist',
                params: {},
                query: {
                    folderid: item.folderid,
                    foldername: escape(item.foldername)
                }
            });
        },
        _loadData: async function _loadData() {
            try {
                var res = await _axios2.default.get('http://api.liujack.com/blog/folder', {
                    params: {}
                });
                if (res.status === 200) {
                    this.folderlist = res.data.data;
                } else {
                    this.$Message.error('获取数据失败');
                }
            } catch (e) {
                console.log(e);
            }
        }
    }
};

/***/ }),

/***/ 647:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { render: function render() {
    var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;
    return _c('div', {
      staticClass: "m-home-wrapper"
    }, [_vm._m(0), _vm._v(" "), _vm.$route.name === 'mhome' ? _c('section', {
      staticClass: "folder-wrapper"
    }, [_c('ul', {
      staticClass: "list-inner-container"
    }, _vm._l(_vm.folderlist, function (item, index) {
      return _c('li', {
        key: index.folderid,
        staticClass: "list-item ib",
        on: {
          "click": function click($event) {
            _vm.viewFolder(item);
          }
        }
      }, [_c('a', {
        staticClass: "ib wf list-link",
        attrs: {
          "href": "javascript:;"
        }
      }, [_c('div', {
        staticClass: "thumbnail"
      }, [_c('div', {
        staticClass: "folder-style"
      }), _vm._v(" "), _c('div', {
        staticClass: "info ellipsis tc"
      }, [_vm._v(_vm._s(item.foldername))])])])]);
    }))]) : _vm._e(), _vm._v(" "), _c('keep-alive', [_c('router-view')], 1)], 1);
  }, staticRenderFns: [function () {
    var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;
    return _c('section', {
      staticClass: "title vmc-1px-b flex-center"
    }, [_c('h1', [_vm._v("IIU-BLOG")])]);
  }] };
module.exports.render._withStripped = true;
if (false) {
  module.hot.accept();
  if (module.hot.data) {
    require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6a72b02e", module.exports);
  }
}

/***/ }),

/***/ 686:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

});