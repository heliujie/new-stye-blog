export default {
	methods: {
		changeStatus(arrAttr, item, attr) {
		    this[arrAttr].forEach((o) => {
		        o[attr] = false
		    })
		    item[attr] = true
		},
		findCurrentValue(arrAttr, status, attr) {
			let result = null
			this[arrAttr].forEach((o) => {
				if (o[status]) {
					result = o[attr]
				}
			})
			return result
		}
	}
}