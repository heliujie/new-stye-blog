
var webpack = require('webpack');
var config = require('./webpack.base.config');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var fs = require('fs')

config.devtool = '#source-map';
config.output.publicPath = '/';
config.output.filename = '[name].js';
config.output.chunkFilename = '[name].chunk.js';

config.plugins = (config.plugins || []).concat([
	new ExtractTextPlugin({
		filename: '[name].css',
		allChunks: true
	}),
	new webpack.optimize.CommonsChunkPlugin({
		name: 'vendors',
		filename: 'vendors.js'
	}),
	new HtmlWebpackPlugin({
		filename: './index.html',
		template: './src/template/index.ejs',
		inject: false
	})
])

// 写入环境变量
fs.open('./src/config/env.js', 'w', function(err, fd){
	var buf = 'export default "development";';
	fs.write(fd, buf, 0, buf.length, 0, function(err, written, buffer){})
})

module.exports = config
