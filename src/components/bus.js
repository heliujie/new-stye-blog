import Vue from 'vue';
export default new Vue({
    data () {
        return {
            directoryFlag: false,
            currentFileContent: '',
            mainnavlist: [{
            	name: '数据可视化',
            	routeName: 'data-visualization',
            	icon: 'connection-bars',
            	id: 1,
                menu: [
                    {
                        name: '1',
                        icon: 'connection-bars',
                        value: 'Chart.js',
                        active: false,
                        opened: false,
                        children: [
                            {
                                name: '1-1',
                                icon: 'stats-bars',
                                value: '柱状图',
                                active: true,
                                routername: 'chartjs-bar'
                            },
                            {
                                name: '1-2',
                                icon: 'stats-bars',
                                value: '曲线图',
                                active: false,
                                routername: 'chartjs-line'
                            },
                            {
                                name: '1-3',
                                icon: 'stats-bars',
                                value: '区域图',
                                active: false,
                                routername: 'chartjs-area'
                            },
                            {
                                name: '1-4',
                                icon: 'stats-bars',
                                value: '其他图',
                                active: false,
                                routername: 'chartjs-other'
                            }
                        ]
                    }
                    // {
                    //     name: '2',
                    //     icon: 'stats-bars',
                    //     value: 'Echarts',
                    //     routername: 'chartjs-b'
                    // }
                ]
            },{
                name: 'vue组件',
                routeName: 'vue-components-directives',
                icon: 'android-apps',
                id: 2,
                menu: [
                    {
                        name: '1',
                        icon: 'social-javascript',
                        value: 'Vuebar',
                        routername: 'vuebar',
                        active: true
                    },
                    {
                        name: '2',
                        icon: 'image',
                        value: 'vue-lazyload',
                        routername: 'vue-lazyload',
                        active: false
                    }

                ]
            },{
                name: '移动端',
                routeName: 'mobile-page',
                icon: 'iphone',
                id: 3,
                menu: [
                    {
                        name: '1',
                        icon: 'iphone',
                        value: '菜鸟物流',
                        routername: 'cainiao',
                        active: true
                    },
                    {
                        name: '2',
                        icon: 'iphone',
                        value: '美团生活',
                        routername: 'mmeituan',
                        active: false
                    },
                    {
                        name: '3',
                        icon: 'iphone',
                        value: '仿锤子科技移动端Web',
                        routername: 'smartisan',
                        active: false
                    },
                    {
                        name: '4',
                        icon: 'image',
                        value: 'vue2.x移动端组件',
                        routername: 'vue-vmc',
                        active: false
                    }
                ]
            }],
            mainnavCurrentIndex: parseInt(window.localStorage.getItem('MAIN_MODULE_INDEX')) || 0,
            tipMenuFlag: false
        }
    }
})