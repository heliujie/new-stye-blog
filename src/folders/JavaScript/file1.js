let article = {};

article.name = `测试文章标题`;

article.contentlist = [
	{
		type: 'title',
		title: '第一阶段',
		tagName: 'h2',
		id: `h2-a`
	},
	{
		type: 'text',
		content: `近日，《空天猎》  制作人同时也是解放军空军人员的张力中校告诉“环球时报”：“解放军空军制作这部电影的初衷是简单的。我们不打算赚钱; 
		我们想用它来告诉人们，特别是青年，解放军空军是什么样的，
		是怎样在这个时代和未来保护国家的。`
	},
	{
		type: 'image',
		scale: 0.6,
		src: 'https://gitlab.com/heliujie/blog-images-manage/raw/master/vue/qqmusic.png'
	},
	{
		type: 'code',
		code: 
`
module.exports = function(dir){
	dir = dir || '.';
	var app = express()
	var router = express.Router()
	app.use('/assets', serveStatic(path.resolve(dir, 'assets')));
	app.use(router);

	router.get('/post/*', function(req,res,next){
		var name = utils.stripExtname(req.params[0])
		var file = path.resolve(dir, '_post', name + '.md');
		var html = utils.renderPost(dir, file);
		res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'})
		res.end(html);
	})
	router.get('/', function(req, res, next){
		var html = utils.renderIndex(dir);
		res.writeHead(200,{'Content-Type':'text/html;charset=utf-8'})
		res.end(html)
	})
	app.listen(3000);
}
`
	},
	{
		type: 'title',
		title: '小标题',
		tagName: 'h3',
		id: `h3-a-a`
	},
	{
		type: 'code',
		code: `
module.exports%20%3D%20function%28dir%29%7B%0A%09dir%20%3D%20dir%20%7C%7C%20%27.%27%3B%0A%09var%20app%20%3D%20express%28%29%0A%09var%20router%20%3D%20express.Router%28%29%0A%09app.use%28%27/assets%27%2C%20serveStatic%28path.resolve%28dir%2C%20%27assets%27%29%29%29%3B%0A%09app.use%28router%29%3B%0A%0A%09router.get%28%27/post/*%27%2C%20function%28req%2Cres%2Cnext%29%7B%0A%09%09var%20name%20%3D%20utils.stripExtname%28req.params%5B0%5D%29%0A%09%09var%20file%20%3D%20path.resolve%28dir%2C%20%27_post%27%2C%20name%20+%20%27.md%27%29%3B%0A%09%09var%20html%20%3D%20utils.renderPost%28dir%2C%20file%29%3B%0A%09%09res.writeHead%28200%2C%7B%27Content-Type%27%3A%27text/html%3Bcharset%3Dutf-8%27%7D%29%0A%09%09res.end%28html%29%3B%0A%09%7D%29%0A%09router.get%28%27/%27%2C%20function%28req%2C%20res%2C%20next%29%7B%0A%09%09var%20html%20%3D%20utils.renderIndex%28dir%29%3B%0A%09%09res.writeHead%28200%2C%7B%27Content-Type%27%3A%27text/html%3Bcharset%3Dutf-8%27%7D%29%0A%09%09res.end%28html%29%0A%09%7D%29%0A%09app.listen%283000%29%3B%0A%7D`
	},
	{
		type: 'text',
		content: `“这个故事主要是反恐，但是我们如何使用像J-20和J-在反恐任务中的10C成为编剧的挑战。因此，我们决定让这个故事变得更大：中亚一个虚构的国家面临着军官以及国内外恐怖主义分子的严重恐怖主义危机。没有应对危机的中亚政府，要求中国发动军事干预拯救国家，维护区域和平。”`
	},
	{
		type: 'image',
		scale: 1,
		src: 'http://p3.pstatp.com/large/39b90003e41fc18f437c'
	},
	{
		type: 'text',
		content: `他还说到过去解放军电影的失败。“不幸的是，过去我们没有认真对待宣传，我们没有制作成功的电影来塑造空军的形象。有趣的是，许多申请成为解放军战斗机飞行员的年轻人是受到美国制造的好莱坞电影《壮志凌云》的启发。因此，我们必须制作一部关于解放军空军的电影，能够触及我们自己人民的心脏。”
（其实。。。《壮志凌云》曾成为当年美国的票房冠军）`
	},
	{
		type: 'image',
		scale: 1,
		src: 'http://p1.pstatp.com/large/39b90003e420cce32255'
	},
	{
		type: 'title',
		title: '第三阶段',
		tagName: 'h2',
		id: `h2-b`
	},
	{
		type: 'text',
		content: `据电影预告片，解放军空军利用先进的飞机和主要作战武器来支持拍摄。包括J-20，J-10C，J-11，J-11B和J-16在内的战斗机可以在拖车中看到。一些国外的战斗机，如美制F-15和法国制造的幻影也与解放军战斗机在空战中展现出来。`
	},
	{
		type: 'image',
		scale: 1,
		src: 'http://p3.pstatp.com/large/39ba0003d3d9d0c7202b'
	},
	{
		type: 'title',
		title: '第四阶段',
		tagName: 'h2',
		id: `h2-c`
	},
	{
		type: 'text',
		content: `除战斗机外，其他类似Y-20重型运输机，KJ-500型预警机和CH-5型监视/罢工无人机等飞机也在拖车上显示。张先生告诉“环球时报”，“《空天猎》和《战狼》之间最大的区别在于后者是由民营电影公司生产的，他们没有人民解放军的支持，所以这两部电影在设备上没有比较。`
	},
	{
		type: 'image',
		scale: 1,
		src: 'http://p1.pstatp.com/large/39b90003e420cce32255'
	},
	{
		type: 'text',
		content: `电影首席策划人陈浩，中国人民解放军空军政治工作局宣传局副局长陈昊上将表示：“过去我们只能使用非常基本的地面武装，如坦克和炮兵，制作战争片。中国军事现代化建设取得了许多显着的成就，现在是我们用激情的武器来制作一部鼓舞人心的电影的时候了。“。`
	},
	{
		type: 'title',
		title: '第五阶段',
		tagName: 'h2',
		id: `h2-d`
	},
	{
		type: 'text',
		content: `但一些网络用户在观看预告片后提出了有关这个故事的问题。预告片显示这个故事是关于反恐怖主义的，但是“战斗机不是为了反恐而制造的，所以这个故事可能不是很可信”。`
	},
	{
		type: 'image',
		scale: 1,
		src: 'http://p3.pstatp.com/large/39b90003e421fd94d27b'
	}
]

export default article;