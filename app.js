
const express=require('express');
const static=require('express-static');
const consolidate=require('consolidate');
const expressRoute=require('express-route');
var server=express();
var history = require('connect-history-api-fallback')

server.engine('html', consolidate.ejs);
server.set('views', './dist');
server.set('view engine', 'html');


server.use(history({
	rewrites: [
		{from: '/', to: '/'},
		{ from:'/article/list', to: '/' },
        {from: '/article/filelist/', to: '/'},
        {from: '/article/file', to: '/'}
	]
}))

server.get('/', (req, res) => {
    res.render('index.html');
});


server.use(static('./dist/'));


server.listen(8087, function(){
  console.log('service running at localhost:8083')
});