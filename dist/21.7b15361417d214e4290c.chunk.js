webpackJsonp([21],{

/***/ 482:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(685)

var Component = __webpack_require__(82)(
  /* script */
  __webpack_require__(612),
  /* template */
  __webpack_require__(646),
  /* scopeId */
  "data-v-6861096e",
  /* cssModules */
  null
)
Component.options.__file = "/Volumes/My Passport/new-style-blog/src/views/m/filelist.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] filelist.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6861096e", Component.options)
  } else {
    hotAPI.reload("data-v-6861096e", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 612:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _axios = __webpack_require__(85);

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    data: function data() {
        return {
            articlelist: []
        };
    },
    created: function created() {
        this._loadData();
    },

    methods: {
        viewFile: function viewFile(item) {
            this.$router.push({
                name: 'mafile',
                params: {},
                query: {
                    folderid: this.$route.query.folderid,
                    foldername: this.$route.query.foldername,
                    fileid: item.fileid,
                    files: this.$route.query.files
                }
            });
        },
        _loadData: async function _loadData() {
            try {
                var res = await _axios2.default.get('http://api.liujack.com/blog/file', {
                    params: { folderid: this.$route.query.folderid }
                });
                if (res.status === 200) {
                    this.articlelist = res.data.data;
                } else {
                    this.$Message.error('获取数据失败');
                }
            } catch (e) {
                console.log(e);
            }
        }
    }
};

/***/ }),

/***/ 646:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = { render: function render() {
    var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;
    return _c('div', {
      staticClass: "filelist-wrap"
    }, [_c('div', {
      staticClass: "list-outer-container pr"
    }, [_c('ul', {
      staticClass: "list-inner-container"
    }, _vm._l(_vm.articlelist, function (item, index) {
      return _c('li', {
        key: index,
        staticClass: "list-item ib"
      }, [_c('a', {
        staticClass: "ib wf",
        attrs: {
          "href": "javascript:;"
        },
        on: {
          "click": function click($event) {
            $event.stopPropagation();
            _vm.viewFile(item);
          }
        }
      }, [_c('div', {
        staticClass: "thumbnail"
      }, [_c('div', {
        staticClass: "doc-style"
      }), _vm._v(" "), _c('div', {
        staticClass: "info tc ellipsis"
      }, [_vm._v(_vm._s(item.filename))])])])]);
    }))])]);
  }, staticRenderFns: [] };
module.exports.render._withStripped = true;
if (false) {
  module.hot.accept();
  if (module.hot.data) {
    require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-6861096e", module.exports);
  }
}

/***/ }),

/***/ 685:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })

});