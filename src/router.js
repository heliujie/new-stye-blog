
import Util from './libs/utils';

const routers = [
    {
        path: '/',
        meta: {
            title: '首页'
        },
        name: 'phome',
        beforeEnter: (to, from, next) => {
             Util.isMobile ? next({name:'mhome'}) : next()
        },
        component: (resolve) => require(['./views/index'], resolve)
    },
    {
        path: '/article/list',
        name: 'alist',
        meta: {
            title: '列表' 
        },
        component: (resolve) => require(['./views/article/list'], resolve)
    },
    {
        path: '/article/file',
        name: 'afile',
        meta: {
            title: '文件'
        },
        component: (resolve) => require(['./views/article/file'], resolve)
    },
    {
        path: '/article/filelist/:folderid',
        name: 'filelist',
        meta: {
            title: ''
        },
        component: (resolve) => require(['./views/article/filelist'], resolve)
    },
    {
        path: '/author',
        name: 'author',
        meta: {
            title: ''
        },
        component: resolve => require(['./views/author'], resolve)
    },
    {
        path: '/demo',
        name: 'demo',
        meta: {
            title: '示例'
        },
        component: (resolve) => require(['./views/demo/index'], resolve),
        children: [
            {
                path: 'cross-domain',
                name: 'cross-domain',
                component: (resolve) => require(['./views/demo/cross-domain/cross-domain'], resolve),
                meta: {
                    title: '跨域问题'
                }
            }
        ]
    },
    {
        name: 'plug',
        path: '/plug',
        meta: {
            title: '插件'
        },
        component: (resolve) => require(['./views/plug/index'], resolve),
        children: [
            {
                name: 'home',
                path: 'home',
                component: (resolve) => require(['./views/plug/home'], resolve),
                children: [
                    {
                        path: 'data-visualization',
                        name: 'data-visualization',
                        redirect: {
                            name: 'chartjs-bar'
                        },
                        component: (resolve) => require(['./views/plug/data-visualization/index'], resolve),
                        meta: {
                            title: '数据可视化'
                        },
                        children: [
                            {
                                meta: {
                                    parent: 'data-visualization',
                                    title: '柱状图'
                                },
                                name: 'chartjs-bar',
                                path: 'chartjs-bar',
                                component: (resolve) => require(['./views/plug/data-visualization/chartjs/bar'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'data-visualization',
                                    title: '曲线图'
                                },
                                name: 'chartjs-line',
                                path: 'chartjs-line',
                                component: (resolve) => require(['./views/plug/data-visualization/chartjs/line'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'data-visualization',
                                    title: '区域图'
                                },
                                name: 'chartjs-area',
                                path: 'chartjs-area',
                                component: (resolve) => require(['./views/plug/data-visualization/chartjs/area'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'data-visualization',
                                    title: '其他图'
                                },
                                name: 'chartjs-other',
                                path: 'chartjs-other',
                                component: (resolve) => require(['./views/plug/data-visualization/chartjs/other'], resolve)
                            }
                        ]
                    },
                    {
                        path: 'vue-components-directives',
                        name: 'vue-components-directives',
                        redirect: {
                            name: 'vuebar'
                        },
                        component: (resolve) => require(['./views/plug/vue-components-directives/index'], resolve),
                        meta: {
                            title: 'vue组件/指令'
                        },
                        children: [
                            {
                                meta: {
                                    parent: 'vue-components-directives',
                                    title: 'Vuebar'
                                },
                                name: 'vuebar',
                                path: 'vuebar',
                                component: (resolve) => require(['./views/plug/vue-components-directives/vuebar/demo'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'vue-components-directives',
                                    title: 'vue-lazyload'
                                },
                                name: 'vue-lazyload',
                                path: 'vue-lazyload',
                                component: (resolve) => require(['./views/plug/vue-components-directives/vue-lazyload/demo'], resolve)
                            }
                        ]
                    },
                    {
                        path: 'mobile-page',
                        name: 'mobile-page',
                        redirect: {
                            name: 'cainiao'
                        },
                        component: (resolve) => require(['./views/pages/index'], resolve),
                        meta: {
                            title: '移动端页面'
                        },
                        children: [
                            {
                                meta: {
                                    parent: 'mobile-page',
                                    title: '菜鸟物流'
                                },
                                name: 'cainiao',
                                path: 'cainiao',
                                component: (resolve) => require(['./views/pages/cainiao/index'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'mobile-page',
                                    title:'美团生活主页'
                                },
                                name: 'mmeituan',
                                path: 'mmeituan',
                                component: (resolve) => require(['./views/pages/mmeituan/index'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'mobile-page',
                                    title: '仿锤子科技移动端Web'
                                },
                                name: 'smartisan',
                                path: 'smartisan',
                                component: (resolve) => require(['./views/pages/smartisan/index'], resolve)
                            },
                            {
                                meta: {
                                    parent: 'vue-components-vmc',
                                    title: 'vue-vmc'
                                },
                                name: 'vue-vmc',
                                path: 'vue-vmc',
                                component: (resolve) => require(['./views/plug/vue-components-directives/vue-vmc/demo'], resolve)
                            }

                        ]
                    }
                ]
            }
        ]
    },
    {
         path: '/m',
         name: 'mhome',
         meta: {
             title: '移动端'
         },
         beforeEnter: (to, from, next) => {
             Util.isMobile ? next() : next({name: 'phome'})
         },
         component: resolve => require(['./views/m'], resolve),
         children: [
             {
                 name: 'mfilelist',
                 path: 'filelist',
                 meta: {
                     title: '列表' 
                 },
                 component: r => require(['./views/m/filelist'], r)
             },
             {
                 name: 'mafile',
                 path: 'afile',
                 meta: {
                     title: '内容' 
                 },
                 component: r => require(['./views/m/file'], r)
             }
         ]
    }
]
export default routers;